<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'gender', 'user_id', 'country'], 'integer'],
            [['name', 'youth_start', 'youth_end', 'fill_status', 'fill_inform', 'fill_job', 'fill_residence'], 'safe'],
            [['count_autoreg', 'time_delay', 'important_delay', 'time_expectations', 'time_attempt', 'repost_count', 'entry_count', 'photo_count'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tasks::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'count_autoreg' => $this->count_autoreg,
            'time_delay' => $this->time_delay,
            'important_delay' => $this->important_delay,
            'time_expectations' => $this->time_expectations,
            'time_attempt' => $this->time_attempt,
            'country' => $this->country,
            'repost_count' => $this->repost_count,
            'entry_count' => $this->entry_count,
            'photo_count' => $this->photo_count,
            'gender' => $this->gender,
            'youth_start' => $this->youth_start,
            'youth_end' => $this->youth_end,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'fill_status', $this->fill_status])
            ->andFilterWhere(['like', 'fill_inform', $this->fill_inform])
            ->andFilterWhere(['like', 'fill_job', $this->fill_job])
            ->andFilterWhere(['like', 'fill_residence', $this->fill_residence]);

        return $dataProvider;
    }
}
