<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_agent".
 *
 * @property int $id
 * @property string $name Наименование
 */
class UserAgent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fileUploading;
    public static function tableName()
    {
        return 'user_agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['fileUploading'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'fileUploading'=>'Выберите файл',
        ];
    }
}
