<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class DispatchStatusExport
 * @package app\models
 */
class AutoregExport extends Model
{
    /**
     * @var $dispatchId
     */
    public $dispatchId;

    /**
     * @var array
     */
    public $statuses;

    /**
     * @var int
     */
    public $count = 1;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatchId', 'statuses', 'count'], 'required'],
            [['count'], 'integer', 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dispatchId' => 'Рассылка',
            'statuses' => 'Даты регистраций',
            'count' => 'Количество',
        ];
    }

    /**
     * @param boolean $runValidation
     * @return string|null
     */
    public function export($runValidation = true)
    {
        if($runValidation) {
            if($this->validate()){
                return null;
            }
        }

            /** @var AutoRegs $model */
            $dispatchStatusFalse = AutoRegs::find()->where(['task_id' => $this->dispatchId, 'export' => false, 'status' => 1])->limit($this->count);
            $orConditions = [];
            foreach ($this->statuses as $status)
            {
                $orConditions[] = ['like', 'register_date', $status];
            }
            $dispatchStatusFalse->andFilterWhere(ArrayHelper::merge(['or'], $orConditions));

            $models = $dispatchStatusFalse->all();

            $filePath = 'export.txt';

            $file = fopen('export.txt', 'w');

            foreach ($models as $model)
            {
                fwrite($file,$model->login.':'.$model->password."\r\n");
                $model->export = true;
                $model->save(false);
            }

            fclose($file);

            return $filePath;
    }

    /**
     * @param integer $taskId
     * @return array
     */
    public static function getDate($taskId = null)
    {
        $all = AutoRegs::find()->where(['export' => false]);

        $all->andFilterWhere(['task_id' => $taskId]);

        $all = $all->all();

        foreach ($all as $item) {
            $d = date('Y-m-d', strtotime($item->register_date));
            $count = AutoRegs::find()->where(['status' => 1, 'export' => false])->andFilterWhere(['between', 'register_date', "$d 00:00:00", "$d 23:59:59"])->count();
            if($count <= 0){
                continue;
            }
            $date[] = [
                'date' => $d,
                'text' => $d.' | Кол-во: '.$count,
            ];
        }
        return $date;
    }

}