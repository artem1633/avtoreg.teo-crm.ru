<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "logs".
 *
 * @property int $id
 * @property string $datetime Дата и время
 * @property int $type Тип
 * @property string $comment Комментарий
 * @property int $user_id Пользовател
 *
 * @property Users $user
 */

class Logs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs';
    }

    public function behaviors()
    {
        if(Yii::$app->user->identity){
            return [
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'user_id',
                    'updatedByAttribute' => null,
                    'value' => function($event) {
                        return Yii::$app->user->identity->id;
                    },
                ],
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datetime'], 'safe'],
            [['type', 'user_id'], 'integer'],
            [['comment'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Дата и время',
            'type' => 'Тип',
            'comment' => 'Комментарий',
            'user_id' => 'Компания',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if($this->datetime == null) $this->datetime = date('Y-m-d H:i:s');
        }
      
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0) {
                $userId = Yii::$app->user->identity->id;
            }
            else $userId = null;
        } 
        else $userId = null;

        return new AppActiveQuery(get_called_class(), [
           'userId' => $userId,
        ]);
    }

    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) 
        {
            if(Yii::$app->user->identity->type !== 0)
            {
                $userId = Yii::$app->user->identity->id;
                if($model->user_id != $userId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }
}
