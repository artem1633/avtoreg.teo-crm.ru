<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accounting_report".
 *
 * @property int $id
 * @property string $date_report Дата и время события
 * @property int $company_id ID компании
 * @property int $operation_type тип операции
 * @property string $amount Сумма оперции
 * @property string $description Описание операции
 *
 * @property Companies $companyModel
 */
class AccountingReport extends \yii\db\ActiveRecord
{
    const TYPE_INCOME_BALANCE = 1; //Пополнение баланса
    const TYPE_INCOME_AFFILIATE = 2; //Партнерские отчисления
    const TYPE_DISPATCH_PAYED = 3; //Оплата за рассылку
    const TYPE_OUTPUT_AFFILIATE = 4; //Вывод партнерских средств
    const TYPE_OUTPUT_SHOP = 5; //Вывод партнерских средств
    const TYPE_SHOP_PAYED = 6; // Оплата магазина
    const TYPE_ERROR = 7; // Оплата магазина

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounting_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_report'], 'safe'],
            [['company_id', 'operation_type', 'amount'], 'required'],
            [['company_id', 'operation_type'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_report' => 'Дата операции',
            'company_id' => 'Компания',
            'operation_type' => 'Тип операции',
            'amount' => 'Сумма',
            'description' => 'Описание'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Users::className(), ['id' => 'company_id']);
    }
}
