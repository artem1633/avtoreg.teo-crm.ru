<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AutoRegs;

/**
 * AutoRegsSearch represents the model behind the search form about `app\models\AutoRegs`.
 */
class AutoRegsSearch extends AutoRegs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'gender', 'proxy_type', 'task_id', 'user_id'], 'integer'],
            [['login', 'password', 'link', 'add_information', 'comment', 'register_date', 'price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoRegs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'gender' => $this->gender,
            'proxy_type' => $this->proxy_type,
            'register_date' => $this->register_date,
            'task_id' => $this->task_id,
            'user_id' => $this->user_id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'add_information', $this->add_information])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
