<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property string $telephone
 * @property int $type
 * @property string $telegram_id
 * @property string $sms_key
 * @property double $one_price
 * @property integer $mobile_proxy
 * @property string $mp_login
 * @property string $mp_password
 *
 * @property AutoRegs[] $autoRegs
 * @property Tasks[] $tasks
 */

class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_password;

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['type','referal_id'], 'integer'],
            [['fio', 'login', 'password', 'telephone', 'telegram_id', 'new_password', 'sms_key', 'mp_login', 'mp_password'], 'string', 'max' => 255],
            [['main_balance', 'partner_balance', 'prosent_referal', 'one_price'], 'number'],
            [['mobile_proxy'], 'safe'],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'type' => 'Должность',
            'password' => 'Пароль',
            'telegram_id' => 'Id чат телеграма',
            'new_password' => 'Новый пароль',
            'telephone' => 'Телефон',
            'main_balance' => 'Баланс основной',
            'partner_balance' => 'Баланс партнерский',
            'referal_id' => 'Реферал',
            'agree' => 'Я согласен с обработкой персональных данных',
            'prosent_referal' => 'Процент с оплат рефералов',
            'sms_key' => 'SMS ключ',
            'one_price' => 'Цена одного',
            'mobile_proxy' => 'Мобильный прокси',
            'mp_login' => 'Мобильный прокси логин',
            'mp_password' => 'Мобильный прокси пароль'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }       
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {        
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalInformations()
    {
        return $this->hasMany(AdditionalInformation::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoRegs()
    {
        return $this->hasMany(AutoRegs::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNames()
    {
        return $this->hasMany(Name::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurnames()
    {
        return $this->hasMany(Surname::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Logs::className(), ['user_id' => 'id']);
    }

    public function getPermissionList()
    {
        return [
            0 => "Супер Администратор",
            1 => "Администратор",
            2 => "Менеджер",
        ];
    }

    public static function getAllInArray()
    {
        $companies = self::find()
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'fio');
    }

    public function getPermission()
    {
        if($this->type == 0) return "Супер Администратор";
        if($this->type == 1) return "Администратор";
        if($this->type == 2) return "Менеджер";
    }

    //Список тарифов.
    public function getTariffList()
    {
        $tariffs = Tariffs::find()->all();
        return ArrayHelper::map( $tariffs , 'id', 'name');
    }
}
