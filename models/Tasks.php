<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "tasks".
 *
 * @property int $id
 * @property string $name Название
 * @property int $status Статус
 * @property double $count_autoreg Кол-во авторегов
 * @property double $time_delay Время задержки между каждым действием бота
 * @property double $important_delay Важные задержки
 * @property double $time_expectations Время ожидания смс
 * @property double $time_attempt Время каждой попытки
 * @property double $repost_count Кол-во репостов
 * @property double $entry_count Кол-во вступлений
 * @property double $photo_count Кол-во фото
 * @property int $gender Пол
 * @property string $youth_start Возраст старт
 * @property string $youth_end Возраст конец
 * @property int $fill_status Заполнять статус
 * @property int $fill_inform Заполнять информация о себе
 * @property int $fill_job Заполнять место работы
 * @property int $fill_residence Заполнять место жительства
 * @property int $country Страна
 *
 * @property AutoRegs[] $autoRegs
 */
class Tasks extends \yii\db\ActiveRecord
{
    const COUNTRY_RU = 0;
    const COUNTRY_UA = 1;
    const COUNTRY_KZ = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasks';
    }

    public function behaviors()
    {
        if(Yii::$app->user->identity){
            return [
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'user_id',
                    'updatedByAttribute' => null,
                    'value' => function($event) {
                        return Yii::$app->user->identity->id;
                    },
                ],
            ];
        }
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'gender', 'fill_status', 'fill_inform', 'fill_job', 'fill_residence', 'user_id', 'country'], 'integer'],
            [['count_autoreg', 'time_delay', 'important_delay', 'time_expectations', 'time_attempt', 'repost_count', 'entry_count', 'photo_count'], 'number'],
            [['youth_start', 'youth_end'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
            'count_autoreg' => 'Кол-во авторегов',
            'time_delay' => 'Время задержки между каждым действием бота',
            'important_delay' => 'Важные задержки',
            'time_expectations' => 'Время ожидания смс',
            'time_attempt' => 'Время каждой попытки',
            'repost_count' => 'Кол-во репостов',
            'entry_count' => 'Кол-во вступлений',
            'photo_count' => 'Кол-во фото',
            'gender' => 'Пол',
            'youth_start' => 'Возраст старт',
            'youth_end' => 'Возраст конец',
            'fill_status' => 'Заполнять статус',
            'fill_inform' => 'Заполнять информация о себе',
            'fill_job' => 'Заполнять место работы',
            'fill_residence' => 'Заполнять место жительства',
            'country' => 'Страна',
            'user_id' => 'Компания',
        ];
    }

    public static function getCountries()
    {
        return [
            self::COUNTRY_RU => 'Россия',
            self::COUNTRY_UA => 'Украина',
            self::COUNTRY_KZ => 'Казахстан',
        ];
    }

    public function getStatus()
    {
        return [
            1 => 'В работе',
            2 => 'Выполнен',
            3 => 'Остановлен',
        ];
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    public function getGender()
    {
        return [
            1 => 'М',
            0 => 'Ж',
        ];
    }

    public function setDefaultValues()
    {
        $this->name = 'Женские';
        $this->status = 1;
        $this->count_autoreg = 20;
        $this->important_delay = 4;
        $this->time_expectations = 130;
        $this->time_attempt = 380;
        $this->repost_count = 3;
        $this->entry_count = 5;
        $this->photo_count = 3;
        $this->time_delay = 3;
        $this->gender = 0;
        $this->youth_start = date('1993-11-29');
        $this->youth_end = date('1995-11-29');
        $this->fill_status = 1;
        $this->fill_inform = 1;
        $this->fill_job = 1;
        $this->fill_residence = 1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoRegs()
    {
        return $this->hasMany(AutoRegs::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0) {
                $userId = Yii::$app->user->identity->id;
            }
            else $userId = null;
        } 
        else $userId = null;

        return new AppActiveQuery(get_called_class(), [
           'userId' => $userId,
        ]);
    }

    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) 
        {
            if(Yii::$app->user->identity->type !== 0)
            {
                $userId = Yii::$app->user->identity->id;
                if($model->user_id != $userId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

}
