<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "name".
 *
 * @property int $id
 * @property string $name Имя
 * @property int $gender Пол
 * @property string $date Дата использования
 * @property int $user_id Пользовател
 *
 * @property Users $user
 */
class Name extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fileUploading;
    public static function tableName()
    {
        return 'name';
    }

    public function behaviors()
    {
        if(Yii::$app->user->identity){
            return [
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'user_id',
                    'updatedByAttribute' => null,
                    'value' => function($event) {
                        return Yii::$app->user->identity->id;
                    },
                ],
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gender', 'user_id'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['fileUploading'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'gender' => 'Пол',
            'date' => 'Дата использования',
            'fileUploading'=>'Выберите файл',
            'user_id' => 'Компания',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date = date('Y-m-d H:i:s');
        }
        if ($this->date != null) $this->date = date('Y-m-d', strtotime($this->date));

        return parent::beforeSave($insert);
    }

    public function getGender()
    {
        return [
            1 => 'М',
            0 => 'Ж',
        ];
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0) {
                $userId = Yii::$app->user->identity->id;
            }
            else $userId = null;
        } 
        else $userId = null;

        return new AppActiveQuery(get_called_class(), [
           'userId' => $userId,
        ]);
    }

    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) 
        {
            if(Yii::$app->user->identity->type !== 0)
            {
                $userId = Yii::$app->user->identity->id;
                if($model->user_id != $userId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }
}
