<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "auto_regs".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $link Ссылка на страница
 * @property int $status Статус доступности
 * @property int $gender Пол
 * @property string $add_information Доп. информация
 * @property int $proxy_type Тип прокси
 * @property string $comment Комментарий
 * @property string $register_date Дата и ввремя регистрации
 * @property int $task_id Задача
 * @property int $export
 * @property string $date_block
 * @property string $photo
 * @property string $username
 * @property int $user_id Пользовател
 * @property double $price Цена
 *
 * @property Tasks $task
 * @property Users $user
 */
class AutoRegs extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_regs';
    }

    public function behaviors()
    {
        if(Yii::$app->user->identity){
            return [
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'user_id',
                    'updatedByAttribute' => null,
                    'value' => function($event) {
                        return Yii::$app->user->identity->id;
                    },
                ],
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link', 'add_information', 'comment', 'photo', 'username'], 'string'],
            [['status', 'gender', 'proxy_type', 'task_id', 'export', 'user_id'], 'integer'],
            [['register_date'], 'safe'],
            [['price'], 'number'],
            [['login', 'password'], 'string', 'max' => 255],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'link' => 'Ссылка на страница',
            'status' => 'Статус доступности',
            'gender' => 'Пол',
            'add_information' => 'Доп. информация',
            'proxy_type' => 'Тип прокси',
            'comment' => 'Комментарий',
            'register_date' => 'Дата и ввремя регистрации',
            'task_id' => 'Задача',
            'export' => 'Ввыгружен',
            'date_block' => 'Дата блокировки',
            'photo' => 'Фото',
            'username' => 'ФИО',
            'user_id' => 'Компания',
            'price' => 'Цена',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if($this->register_date == null) $this->register_date = date('Y-m-d H:i:s');
        }
      
        return parent::beforeSave($insert);
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function getGender()
    {
        return [
            1 => 'М',
            0 => 'Ж',
        ];
    }

    public function getStatus()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

    public function getTasksList()
    {
        $tasks = Tasks::find()->all();
        return ArrayHelper::map( $tasks , 'id', 'name');
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0) {
                $userId = Yii::$app->user->identity->id;
            }
            else $userId = null;
        } 
        else $userId = null;

        return new AppActiveQuery(get_called_class(), [
           'userId' => $userId,
        ]);
    }

    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) 
        {
            if(Yii::$app->user->identity->type !== 0)
            {
                $userId = Yii::$app->user->identity->id;
                if($model->user_id != $userId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }
}
