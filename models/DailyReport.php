<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "daily_report".
 *
 * @property int $id
 * @property string $date_event Дата и время события
 * @property int $user_id ID компании
 * @property string $type Тип события
 * @property int $task_id
 * @property string $comment Комментарии
 * @property string $comment2 Комментарии
 *
 */
class DailyReport extends \yii\db\ActiveRecord
{
    const TYPE_SENT = 1; // отправлено сообщений
    const TYPE_READ = 2; // прочитано
    const TYPE_USER_STATUS = 3; // пользовательский статус
    const TYPE_BLOCK_REGIST = 4; // Блокировка бота
    const TYPE_PAY_ERROR = 5; // Оплата по ошибки
    const TYPE_VISIT = 6; // пользователь зашол сегодня

    /**
     * @var string
     */
    public $dates;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_event'], 'safe'],
            [['comments'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_event' => 'Дата и время события',
            'user_id' => 'ID Компании',
            'task_id' => 'ID задачи',
            'type' => 'Тип события',
            'comment' => 'Комментарии',
            'comment2' => 'Комментарии'
        ];
    }


    /**
     * Производит поиск
     * @return array
     */
    public function search()
    {
        $report = [];
        $report[1] = [];//кол-во регистраций
        $report[2] = [];//кол-во операций
        $report[3] = [];//сумма операций
        $report[4] = [];//кол-во блокир акк
        $report[5] = [];//кол-во онлайн
        $report[6] = [];//кол-во купленых акк
        $report[7] = [];//кол-во отправленых  сообщений
        $report[8] = [];//список компания кто пополнил

        if($this->dates != null) {
            $this->dates = explode(' - ', $this->dates);
            $dateStart = $this->dates[0];
            $dateEnd = $this->dates[1];
        } else {
            $year = date('Y');
            $month = date('m');

            $lastDayMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year) + 1;

            $dateStart = date('Y-m-1');
            $dateEnd = date('Y-m-'.$lastDayMonth);
            for ($i = 1; $i < $lastDayMonth; $i++){
                $a [] = date('Y-m-'.$i);
            }

            $report[0] = $a;
        }

//        $query = DailyReport::find();
//
//        $query->andFilterWhere(['between', 'date_event', $dateStart, $dateEnd]);
//
//        $report[0] = array_reverse(array_unique(ArrayHelper::getColumn($query->orderBy('date_event desc')->all(), 'date_event')));

        foreach ($report[0] as $date)
        {
            $date = date("Y-m-d", strtotime($date));
            $report[1][] = intval(AutoRegs::find()->where(['like','register_date',$date])->count());
            $report[3][] = intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])->andfilterWhere(['=','operation_type','7'])->sum('amount'));
            $report[4][] = intval(AutoRegs::find()->where(['like','date_block',$date])->andWhere(['export' => false])->count());
            $report[5][] = intval(AutoRegs::find()->where(['like','date_block',$date])->andWhere(['export' => false])->sum('price'));
            $report[6][] = intval(AutoRegs::find()->where(['like','register_date',$date])->sum('price'));

        }
        $report[7] = array_sum($report[5]) + array_sum($report[3]);

        return $report;
    }

}
