<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "additional_information".
 *
 * @property int $id
 * @property int $type Тип
 * @property string $value Значение
 * @property int $count Кол-во раз использовалась
 * @property string $last_date Дата и время последнего использования
 * @property int $user_id Пользовател
 *
 * @property Users $user
 */
class AdditionalInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fileUploading;
    public static function tableName()
    {
        return 'additional_information';
    }

    public function behaviors()
    {
        if(Yii::$app->user->identity){
            return [
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'user_id',
                    'updatedByAttribute' => null,
                    'value' => function($event) {
                        return Yii::$app->user->identity->id;
                    },
                ],
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'count', 'user_id', 'group'], 'integer'],
            [['last_date'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['fileUploading'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'value' => 'Значение',
            'count' => 'Кол-во раз использовалась',
            'last_date' => 'Дата и время последнего использования',
            'fileUploading'=>'Выберите файл',
            'user_id' => 'Компания',
            'group' => 'Группа'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->last_date = date('Y-m-d H:i:s');
        }
      
        return parent::beforeSave($insert);
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    public function getType()
    {
        return [
            1 => 'Заполнение статуса',
            2 => 'Заполнение инфа о себе',
            3 => 'Место работы',
            4 => 'Место Жительста',
            5 => 'Репосты',
            6 => 'Вступления в группу',
            7 => 'Фотография',
        ];
    }

    public function getTypeCode($value)
    {
        if('Заполнение статуса' == $value) return 1;
        if('Заполнение инфа о себе' == $value) return 2;
        if('Место работы' == $value) return 3;
        if('Место Жительста' == $value) return 4;
        if('Репосты' == $value) return 5;
        if('Вступления в группу' == $value) return 6;
        if('Фотография' == $value) return 7;
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0) {
                $userId = Yii::$app->user->identity->id;
            }
            else $userId = null;
        } 
        else $userId = null;

        return new AppActiveQuery(get_called_class(), [
           'userId' => $userId,
        ]);
    }

    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) 
        {
            if(Yii::$app->user->identity->type !== 0)
            {
                $userId = Yii::$app->user->identity->id;
                if($model->user_id != $userId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }
}
