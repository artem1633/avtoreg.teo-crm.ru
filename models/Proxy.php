<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proxy".
 *
 * @property int $id
 * @property string $ip Ip
 * @property int $type Тип
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $source Источник
 */
class Proxy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fileUploading;

    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['fileUploading'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
            [['ip', 'login', 'password', 'source'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'type' => 'Тип',
            'login' => 'Логин',
            'password' => 'Пароль',
            'source' => 'Источник',
            'fileUploading'=>'Выберите файл',
        ];
    }
}
