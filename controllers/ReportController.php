<?php

namespace app\controllers;

use app\filters\PermissionsFilter;
use app\models\DailyReport;
use app\models\RegistrationReportFilter;
use app\models\Users;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * GlobalMessagesController implements the CRUD actions for GlobalMessages model.
 */
class ReportController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GlobalMessages models.
     * @return mixed
     */
    public function actionReg()
    {
        $request = Yii::$app->request;
        $model = new DailyReport();

        $model->load($request->get());

        $report = $model->search();

        return $this->render('reg', [
            'report' => $report,
            'model' => $model
        ]);
    }
    /**
     * Lists all GlobalMessages models.
     * @return mixed
     */
    public function actionVisit()
    {
//        $model = DailyReport::find()->where(['type' => 5])->select(['comments'])->distinct()->all();
        $query = DailyReport::find()->where(['type' => 5])->select(['comments'])->distinct();
        $model = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(['!=', 'comments', 'Пользователь первый раз зашол']);

        return $this->render('visit', [
            'model' => $model
        ]);
    }
}
