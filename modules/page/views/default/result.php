<?php

use yii\helpers\Html;
use app\models\Questionary;
use yii\widgets\Pjax;
use app\models\SettingResult;

$_csrf = \Yii::$app->request->getCsrfToken();
$session = Yii::$app->session;

$settingResults = SettingResult::find()->where(['questionary_id' => $session['resume']->questionary->id])->all();
$ball = $session['resume']->balls + $model->ball_for_question;
?>      

<div class="anketa-container lightmode">        
    <div class="anketa-box animated fadeInDown">
        <div class="anketa-body" >
	        <h1 style="color: #fff !important;">Анкета: <?=$session['resume']->questionary->name?></h1>
	        <br>
	        <h2 style="color: #fff !important;">Ваш результат: <?=$session['resume']->balls + $model->ball_for_question?> балл</h2>
	        <br>
	       <!--  <h2 style="color: #fff !important;">Время которое ушло на тест: <?$php //session['resume']->getTime()?></h2> -->
	       <?php 
	       	foreach ($settingResults as $value) {
	       		if($value->condition == 1 && $ball > $value->first_value) echo "<br> {$value->text}";
	       		if($value->condition == 2 && $ball >= $value->first_value) echo "<br> {$value->text}";
	       		if($value->condition == 3 && $ball < $value->first_value) echo "<br> {$value->text}";
	       		if($value->condition == 4 && $ball <= $value->first_value) echo "<br> {$value->text}";
	       		if($value->condition == 5 && $value->first_value < $ball && $ball < $value->second_value) echo "<br> {$value->text}";
	       		if($value->condition == 6 && $value->first_value <= $ball && $ball <= $value->second_value) echo "<br> {$value->text}";
	       		if($value->condition == 7 && $value->first_value <= $ball && $ball < $value->second_value) echo "<br> {$value->text}";
	       		if($value->condition == 8 && $value->first_value < $ball && $ball <= $value->second_value) echo "<br> {$value->text}";
	       		if($value->condition == 9 && $value->first_value == $ball) echo "<br> {$value->text}";
	       	}
	       ?>
    	</div>
	</div>
</div>