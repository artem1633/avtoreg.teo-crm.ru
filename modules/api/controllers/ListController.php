<?php

namespace app\modules\api\controllers;

use app\models\AccountingReport;
use app\models\AdditionalInformation;
use app\models\DailyReport;
use app\models\Settings;
use app\models\Tasks;
use app\models\Users;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\Name;
use app\models\UserAgent;
use app\models\Proxy;
use yii\web\Response;
use app\models\Logs;
use app\models\Surname;
use app\models\AutoRegs;

/**
 * Default controller for the `api` module
 */
class ListController extends Controller
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['names', 'user-agent','task', 'proxy', 'surnames', 'logs', 'auto-regs', 'akk-info'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [ ]);
    }
    public function actionProxy()
    {
        $proxy = Proxy::find()->all();
        $result = [];
        foreach ($proxy as $value) {
            $result [] = [
                //'id' => $value->id,
                'ip' => $value->ip,
                'type' => $value->type,
                'login' => $value->login,
                'password' => $value->password,
                'source' => $value->source,
            ];
        }
        return $result;
    }
    public function actionTask()
    {
        $user_push =  explode(',', Settings::findByKey('user_push')->value);

        /** @var Tasks $task */
        $taskAll =  ArrayHelper::getColumn(Tasks::find()->where(['status' => 1])->all(), 'id');
        if (!$taskAll) {
//            self::sendTelMessage($user_push, "нету заданий");
            return 'нету заданий';
        }
        $idT = $taskAll[rand(0, count($taskAll)-1)];
        $task = Tasks::findOne($idT);
        if (!$task) {
//            self::sendTelMessage($user_push, "нету заданий");
            return 'нету заданий';
        }

        $key_sms = Settings::findByKey('key_sms')->value;
        $inf = file_get_contents('https://smshub.org/stubs/handler_api.php?api_key='.$key_sms.'&action=getBalance');
        $balans = substr($inf,strpos($inf,':') + 1);
        if ($balans < 10) {
//            self::sendTelMessage($user_push, "нету баланса на смс {$balans}");
            return 'нету баланса';
        }

        $key_pr = Settings::findByKey('mobile_proxies')->value;
        if ($key_pr == 1) {
            $key_l = Settings::findByKey('mobile_proxies_login')->value;
            $key_p = Settings::findByKey('mobile_proxies_password')->value;
            $proxy = file_get_contents("https://dashboard.airsocks.in/partner?u={$key_l}&p={$key_p}");
            $proxy = json_decode($proxy);
            foreach ($proxy->data as $item) {
                $proxyAll[] = $item->hostIp.':'.$item->ports->http;
            }
            $proxy = $proxyAll[rand(0, count($proxyAll)-1)];
        } else {
            $proxyAll = ArrayHelper::getColumn(Proxy::find()->where(['type' => 1])->all(), 'id');
            if (!$proxyAll) {
//                self::sendTelMessage($user_push, "нету прокси");
                return 'нету прокси';
            }
            $idP = $proxyAll[rand(0, count($proxyAll)-1)];
            $proxy = Proxy::findOne($idP);
            if (!$proxy) {
//                self::sendTelMessage($user_push, "нету прокси");
                return 'нету прокси';
            }
            $proxy = $proxy->ip;
        }

//        $proxy->type = 2;
//        $proxy->save(false);

        $ids = ArrayHelper::getColumn(Name::find()->where(['user_id' => $task->user_id])->all(), 'id');
        if(count($ids) > 0){
            $id = $ids[rand(0, count($ids)-1)];
        } else {
//            self::sendTelMessage($user_push, "Нету имен у компании {$task->user_id}");
            exit();
        }
        $Ad = Name::find()->where(['id' => $id])->one();
        $itemN = $Ad->name;
        $Ad->date = date('Y-m-d H:m:s');
        $Ad->save(false);



        $ids = ArrayHelper::getColumn(Surname::find()->all(), 'id');
        if(count($ids) > 0){
            $id = $ids[rand(0, count($ids)-1)];
        } else {
//            self::sendTelMessage($user_push, "Нету фамили у компании {$task->user_id}");
            exit();
        }
        $Ad = Surname::find()->where(['id' => $id])->one();
        $itemF = $Ad->surname;
        $Ad->date = date('Y-m-d H:m:s');
        $Ad->save(false);


        $itemS = '';
        if ($task->fill_status) {
            $ids = ArrayHelper::getColumn(AdditionalInformation::find()->where(['type' => 1])->all(), 'id');
            if(count($ids) > 0){
                $id = $ids[rand(0, count($ids)-1)];
            } else {
//                self::sendTelMessage($user_push, "Нету статусов {$task->user_id}");
                exit();
            }
            $Ad = AdditionalInformation::find()->where(['id' => $id])->one();
            $itemS = $Ad->value;
            $Ad->count = $Ad->count + 1;
            $Ad->save(false);
        }

        $itemRes = '';
        if ($task->fill_residence) {
            $ids = ArrayHelper::getColumn(AdditionalInformation::find()->where(['type' => 4])->all(), 'id');
            if(count($ids) > 0){
                $id = $ids[rand(0, count($ids)-1)];
            } else {
//                self::sendTelMessage($user_push, "Нету гоорода {$task->user_id}");
                exit();
            }
            $Ad = AdditionalInformation::find()->where(['id' => $id])->one();
            $itemRes = $Ad->value;
            $Ad->count = $Ad->count + 1;
            $Ad->save(false);
        }


        $itemR = [];
        for ($i = 0; $i < $task->repost_count; $i++){
            $ids = ArrayHelper::getColumn(AdditionalInformation::find()->where(['type' => 5])->all(), 'id');
            if(count($ids) > 0){
                $id = $ids[rand(0, count($ids)-1)];
            } else {
//                self::sendTelMessage($user_push, "Нету для репостов {$task->user_id}");
                exit();
            }
            $Ad = AdditionalInformation::find()->where(['id' => $id])->one();
            $itemR[] = $Ad->value;
            $Ad->count = $Ad->count + 1;
            $Ad->save(false);
        }

        $itemE = [];
        for ($i = 0; $i < $task->entry_count; $i++){
            $ids = ArrayHelper::getColumn(AdditionalInformation::find()->where(['type' => 6])->all(), 'id');
            if(count($ids) > 0){
                $id = $ids[rand(0, count($ids)-1)];
            } else {
//                self::sendTelMessage($user_push, "Нету для вступлений {$task->user_id}");
                exit();
            }
            $Ad = AdditionalInformation::find()->where(['id' => $id])->one();
            $itemE[] = $Ad->value;
            $Ad->count = $Ad->count + 1;
            $Ad->save(false);
        }

        $groups = ArrayHelper::getColumn(AdditionalInformation::find()->where(['type' => 7])->groupBy('group')->all(), 'group');
        if(count($groups) > 0){
            $groupIds = $groups[rand(0, count($groups)-1)];
            $Ad = AdditionalInformation::find()->where(['group' => $groupIds])->all();
            foreach ($Ad as $ad){
                $itemP[] = $ad->value;
                $ad->count = $ad->count + 1;
                $ad->save(false);
            }
        } else {
//            self::sendTelMessage($user_push, "Нету фото {$task->user_id}");
            return "Нету фото";
        }
        $difference = intval(abs(
            strtotime($task->youth_start) - strtotime($task->youth_end)
        ));

        // Количество дней
        $dateHappy = intval(rand(0, $difference) / (3600 * 24));

        $dateHappy = date('Y-m-d', strtotime($task->youth_end."-{$dateHappy} days"));

        $ids = ArrayHelper::getColumn(UserAgent::find()->all(), 'id');
        if(count($ids) > 0){
            $id = $ids[rand(0, count($ids)-1)];
        }
        $Ad = UserAgent::find()->where(['id' => $id])->one();
        $itemU[] = $Ad->name;

        //        $itemAll['items'] = [$itemS,$itemG,$itemP];
        $countLost = AutoRegs::find()->where(['task_id' => $task->id])->count();
        if ($countLost) {
            $countLost = $task->count_autoreg - $countLost;
        } else {
            $countLost = $task->count_autoreg;
        }
        if ($countLost <= 0) {
            $task->status = 2;
            $task->save(false);
//            self::sendTelMessage($user_push, "Задача {$task->name} Завершена");
            return "Задача {$task->name} Завершена";
        }
        $result['setting'] = [
            'id' => $task->id,
            'key_sms' => $key_sms,
            'count_autoreg' => $countLost,
            'country' => $task->country,
            'time_delay' => $task->time_delay,
            'important_delay' => $task->important_delay,
            'time_expectations' => $task->time_expectations,
            'time_attempt' => $task->time_attempt,
            'repost_count' => $task->repost_count,
            'entry_count' => $task->entry_count,
            'photo_count' => $task->photo_count,
            'gender' => $task->gender,
            'birthday' => $dateHappy,
            'fill_status' => $task->fill_status,
            'fill_inform' => $task->fill_inform,
            'fill_job' => $task->fill_job,
            'fill_residence' => $task->fill_residence,
            'status' => $itemS,
            'residence' => $itemRes,
            'photo' => $itemP,
            'entry' => $itemE,
            'repost' => $itemR,
            'name' => $itemN,
            'surname' => $itemF,
            'proxy' => $proxy,
            'useragent' => $itemU,
        ];



//        self::sendTelMessage($user_push, "Запрос к задаче {$task->name} - {$task->id}");
        return $result;
    }


    public function actionLogs()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $log = new Logs();
        $log->type = $request->post('type');;
        $log->comment = $request->post('comment');
        $log->datetime = date('Y-m-d H:m:s');

        if ($log->type == 'warn' or $log->type == 'error') {
            $user_push =  explode(',', Settings::findByKey('user_push')->value);
//            self::sendTelMessage($user_push, "Лог {$log->type} - {$log->comment}");
        }

        if($log->save(false)) {
            return ['message' => 'Успешно выполнено'];
        } else {
            $a = serialize($log->errors);
//            self::sendTelMessage($user_push, "Error {$a}");
            return $log->errors;
        }
    }


    public function actionAutoRegs()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $task = Tasks::findOne($request->post('task_id'));
        $company = Users::findOne($task->user_id);

        $reg = new AutoRegs();
        $reg->task_id = $request->post('task_id');
        $reg->login = $request->post('login');
        $reg->password = $request->post('password');
        $reg->link = $request->post('link');
        $reg->status = $request->post('status');
        $reg->gender = $request->post('gender');
        $reg->add_information = $request->post('add_information');
        $reg->proxy_type = (int) $request->post('proxy_type');
        $reg->comment = $request->post('сomment');
        $reg->register_date = date('Y-m-d H:i:s');
        $reg->user_id = $task->user_id;
        $reg->price = $request->post('price');

        $user_push =  explode(',', Settings::findByKey('user_push')->value);
//        self::sendTelMessage($user_push, "Новый авторег {$reg->login}:{$reg->password} - {$reg->link} - {$reg->price}р");

        if($reg->save(false)){

            $key_pr = Settings::findByKey('mobile_proxies')->value;
            if ($key_pr == 0) {
                $proxy = Proxy::find()->where(['ip' => $request->post('proxy')])->one();
                $proxy->type = 2;
                $proxy->save(false);
            }


            $price = Settings::findByKey('cost_autoreg')->value;
            $pay = new AccountingReport([
                'date_report' => date('Y-m-d H:i:s'),
                'company_id' => $task->user_id,
                'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                'amount' => $price,
                'description' => "За регистрацию акк {$reg->login} себе стоимость {$reg->price}"
            ]);

            if (!$pay->save(false)){
                $a = serialize($pay->errors);
//                self::sendTelMessage($user_push, "Ошибка {$a}");
                return $pay->errors;
            }

            $company->main_balance = $company->main_balance - $price;
            if (!$company->save(false)){
                $a = serialize($company->errors);
//                self::sendTelMessage($user_push, "Ошибка {$a}");
                return $company->errors;
            }

            return ['message' => 'Успешно выполнено'];
        } else {
            $a = serialize($reg->errors);
//            self::sendTelMessage($user_push, "Ошибка {$a}");
            return $reg->errors;
        }
    }

    public function actionPay()
    {
        $user_push =  explode(',', Settings::findByKey('user_push')->value);

        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $pay = new AccountingReport([
            'date_report' => date('Y-m-d H:i:s'),
            'company_id' => 1,
            'operation_type' => AccountingReport::TYPE_ERROR,
            'amount' => $request->post('price'),
            'description' => "Что то пошло не так"
        ]);

        if (!$pay->save(false)){
            $a = serialize($pay->errors);
//            self::sendTelMessage($user_push, "Ошибка {$a}");
            return $pay->errors;
        }

        $task = Tasks::findOne($request->post('task_id'));

        if ($task) {
            $day = new DailyReport([
                'user_id' => $task->user_id,
                'task_id' => $task->id,
                'type' => DailyReport::TYPE_PAY_ERROR,
                'comment' => $request->post('price'),
                'comment2' => 'Комментарии'
            ]);
            if (!$day->save(false)){
//                self::sendTelMessage($user_push, "error day {$day->errors}");
            }
        } else {
//            self::sendTelMessage($user_push, "Не нашли задачу {$request->post('task_id')}");
        }

//        self::sendTelMessage($user_push, "Оплата по ошибки {$request->post('price')} - Задача {$request->post('task_id')}");
        return ['message' => 'Успешно выполнено'];

    }


    public function actionUserAgent()
    {
        $user_agents = UserAgent::find()->all();
        $result = [];
        foreach ($user_agents as $value) {
            $result [] = [
                //'id' => $value->id,
                'name' => $value->name,
            ];
        }
        return $result;
    }

    public function actionNames()
    {
        $names = Name::find()->all();
        $result = [];
        foreach ($names as $value) {
            $result [] = [
                //'id' => $value->id,
                'name' => $value->name,
            ];
        }
        return $result;
    }

    public function actionSurnames()
    {
        $surname = Surname::find()->all();
        $result = [];
        foreach ($surname as $value) {
            $result [] = [
                //'id' => $value->id,
                'surname' => $value->surname,
            ];
        }
        return $result;
    }


    public static function sendTelMessage($userId, $text)
    {
        $token = Settings::findByKey('telegram_key')->value;
        $proxy_server = Settings::findByKey('telegram_proxy')->value;
        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text]);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text]);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }
//        var_dump($proxy_server);
//        var_dump($url);
//
//        var_dump(json_decode($curl_scraped_page, true));

        return json_decode($curl_scraped_page);
    }


    /**
     * @param array $data
     * @return mixed
     */
    public static function actionTest()
    {
        $data = [
            'type' => '1',
            'comment' => 'adfsf',
        ];
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://avtoreg.teo-crm.ru/api/list/logs');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }


    /**
     * @param array $data
     * @return mixed
     */
    public static function actionTest2()
    {

//        $user_push =  explode(',', Settings::findByKey('user_push')->value);
        return self::sendTelMessage('247187885', "1111");
    }

    public function actionAkkInfo()
    {
        $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
        $proxy = json_decode($proxy);
        foreach ($proxy->data as $item) {
            $proxyAll[] = $item->hostIp.':'.$item->ports->http;
        }
        $setingProxy = $proxyAll[rand(0, count($proxyAll)-1)];

        $setingToken = '23a2e5f9073c4b232aca163bb04759852f6927af4b0cd85939d743db61b3b2c2d93a82b899c9527b5e50c';
        $allAkk = AutoRegs::find()->where(['status' => '1'])->all();
        $list = '';
        /** @var DispatchStatus $item */
        foreach ($allAkk as $item) {
            var_dump($item->link);
            $a1 = stripos($item->link, 'id' ) + 2;
            $list .= substr($item->link, $a1).',';
        }
//        var_dump($list);
        $results = $this->getUser($list, 'photo_100,city,country,bdate,sex,domain', $setingToken, $setingProxy->value);
//        var_dump($results);
        foreach ($results['response'] as $item2) {

            $user = AutoRegs::find()
                ->where(['or', ['LIKE', 'link', strval($item2['id'])]])
                ->one();
            if (!$user) {
                continue;
            }
            if($item2['deactivated'] == 'deleted' || $item2['deactivated'] == 'banned')
            {
                $user->status = 0;
                $user->date_block = date('Y-m-d H:m:s');
                $user->save(false);
            }
            if (!$user->username) {
                $user->username = $item2['first_name'];
                $user->photo = $item2['photo_100'];
                $user->save(false);
            }
        }
        return $list;
    }

    public function actionPhotoString()
    {
        $strs = file_get_contents('foto.txt');

        $strs = explode('//////', $strs);

        $groupId = 1;
        for($i = 0; $i < count($strs); $i++){
            $string = $strs[$i];

            if($i == 0){
                $info = AdditionalInformation::find()->where(['type' => 7])->orderBy('id desc')->one();
                if($info){
                    $groupId = $info->group + 1;
                } else {
                    $groupId = 1;
                }
            } else {
                $groupId++;
            }

            $string = explode("\n", $string);

            foreach ($string as $link) {
                if($link == ''){
                    continue;
                }
                (new AdditionalInformation(['group' => $groupId, 'value' => $link, 'type' => 7]))->save(false);
            }

        }
    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    static function getUser($users, $fields, $token, $proxy)
    {
        $url = 'https://api.vk.com/method/users.get';
        $params = array(
            'user_ids' => $users,
            'fields' => $fields,
            'name_case' => 'Nom', // Токен того кто отправляет
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.85',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);

    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    public static function actionNewProxy()
    {
        $key_pr = Settings::findByKey('mobile_proxies')->value;
        if ($key_pr == 0) {
            $url = 'http://htmlweb.ru/json/proxy/get?country=RU&perpage=20';

            // В $result вернется id отправленного сообщения
            Proxy::deleteAll();
            $result = file_get_contents($url);
            $result = json_decode($result, true);
            if (count($result) < 10) {
                $user_push =  explode(',', Settings::findByKey('user_push')->value);
                $a = serialize($proxy->errors);
//                self::sendTelMessage($user_push, "Обновлениее прокси вернуло {$a}");
                exit();
            }
            foreach ($result as $item) {
                if ($item['name']) {
                    $proxy = new Proxy([
                        'ip' => $item['name'],
                        'type' => 1,
                        'source' => 'https://htmlweb.ru/json/proxy/get?country=RU',
                    ]);
                    if (!$proxy->save()) {
                        $user_push =  explode(',', Settings::findByKey('user_push')->value);
                        $a = serialize($proxy->errors);
//                        self::sendTelMessage($user_push, "Ошибка {$a}");
                        exit();
                    }
                }
            }
        }

        return true;
    }


}
