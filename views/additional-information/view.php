<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalInformation */
?>
<div class="additional-information-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'value',
            'count',
            'last_date',
        ],
    ]) ?>

</div>
