<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdditionalInformation */

?>
<div class="additional-information-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
