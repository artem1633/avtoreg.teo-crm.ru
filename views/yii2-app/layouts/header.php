<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use yii\helpers\Html;
use app\models\Settings;
use app\models\Resume;
use yii\helpers\Url;
use app\models\Questionary;
use app\models\Chat;

    $key_sms = Yii::$app->user->identity->sms_key;
    $inf = file_get_contents('https://smshub.org/stubs/handler_api.php?api_key='.$key_sms.'&action=getBalance');
    $balans = substr($inf,strpos($inf,':') + 1). ' SMS '.Yii::$app->user->identity->main_balance;
?>


<!-- START X-NAVIGATION VERTICAL -->
<ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button">
        <a href="#" onclick="$.post('/site/menu-position');" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
    </li>


    <li class="xn-icon-button pull-right">
        <?= Html::a('<span class="fa fa-sign-out"></span>',['/site/logout'],['data-confirm'=> false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',]) ?>
    </li>

    <li class="pull-right">
        <?=Html::a($balans. ' <i class="fa fa-rub"></i>', 'https://sms-activate.ru/',
            [ 'target' => '_blank',  'data' => ['pjax' => 0]])?>
    </li>
</ul>

<section class="breadcrumb push-down-0">

    <?php if (isset($this->blocks['content-header'])) { ?>
        <h1><?php $this->blocks['content-header'] ?></h1>
    <?php } else { ?>
    <?php } ?>

</section>

<div class="page-content-wrap">
    <br>
</div>
