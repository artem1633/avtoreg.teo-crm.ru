<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

$path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
$pathInfo = Yii::$app->request->pathInfo;
$class = "";
if($pathInfo == 'name/index' || $pathInfo == 'surname/index' || $pathInfo == 'additional-information/index' || $pathInfo == 'user-agent/index') $class="active";

$classReport = "";
if($pathInfo == 'users/dashboard' || $pathInfo == 'logs/index' || $pathInfo == '') $classReport = "active";
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-tasks"></span> <span class="xn-text">Задачи</span>', ['/tasks/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-asterisk"></span> <span class="xn-text">Автореги</span>', ['/auto-regs/index'], []); ?>   
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-credit-card"></span> <span class="xn-text">Финансы</span>', ['/payment/index'], []); ?>   
                    </li>
                    <?php if(Yii::$app->user->identity->type == 0) { ?>
                    <li>
                        <?= Html::a('<span class="glyphicon glyphicon-screenshot"></span> <span class="xn-text">Прокси</span>', ['/proxy/index'], []); ?>   
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-cogs"></span> <span class="xn-text">Настройки</span>', ['/settings/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/users/index'], []); ?>
                    </li>
                        <li class="xn-openable <?=$classReport?>"  >
                            <a href="#"><span class="fa fa-bar-chart-o"></span><span class="xn-text">Отчеты</span></a>
                            <ul>
                                <?php if(Yii::$app->user->identity->type == 0) { ?>
                                    <li <?= ($pathInfo == 'users/dashboard' || $pathInfo == '' ? 'class="active"' : '')?> >
                                        <?= Html::a('<span class="fa fa-desktop"></span> Регистрации', ['/report/reg'], []); ?>
                                    </li>
                                <?php } ?>
                                <li <?= ($pathInfo == 'users/dashboard' || $pathInfo == '' ? 'class="active"' : '')?> >
                                    <?= Html::a('<span class="fa fa-desktop"></span> Показатели', ['/users/dashboard'], []); ?>
                                </li>
                                <li <?= ($pathInfo == 'logs/index' ? 'class="active"' : '')?> >
                                    <?= Html::a('<span class="fa fa-eraser"></span>Логи', ['/logs/index'], []); ?>
                                </li>
                            </ul>
                        </li>
                        <li class="xn-openable <?=$class?>"  >
                            <a href="#"><span class="fa fa-bar-chart-o"></span><span class="xn-text">Справочники</span></a>
                            <ul>
                                <li <?= ($pathInfo == 'name/index' ? 'class="active"' : '')?> >
                                    <?= Html::a('<span class="fa fa-asterisk"></span>Имена', ['/name/index'], []); ?>
                                </li>
                                <li <?= ($pathInfo == 'surname/index' ? 'class="active"' : '')?> >
                                    <?= Html::a('<span class="fa fa-list-alt"></span>Фамилия', ['/surname/index'], []); ?>
                                </li>
                                <li <?= ($pathInfo == 'additional-information/index' ? 'class="active"' : '')?> >
                                    <?= Html::a('<span class="fa fa-info"></span>Дополнительная информация', ['/additional-information/index'], []); ?>
                                </li>
                                <?php if(Yii::$app->user->identity->type == 0) { ?>
                                    <li <?= ($pathInfo == 'user-agent/index' ? 'class="active"' : '')?> >
                                        <?= Html::a('<span class="fa fa-eraser"></span>Юзер агент', ['/user-agent/index'], []); ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->