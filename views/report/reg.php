<?php

use app\models\AccountingReport;
use app\models\Companies;
use app\models\DailyReport;
use skeeks\widget\highcharts\Highcharts;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;


/**
 * @var $report array
 * @var $model \app\models\RegistrationReportFilter
 */

//echo "<pre>";
//print_r($report);
//echo "</pre>"

?>


<div class="panel panel-success">
    <div class="panel-heading">
        <div class="panel-title">Отчет о регистрации</div>
    </div>
    <div class="panel-body">


        <?= Highcharts::widget([
            'options' => [
                'chart' => [
                    'height' => 225,
                    'type' => 'line'

                ],
                'title' => false,//['text' => 'Всего отправлено сообщений'],
                //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                'plotOptions' => [
                    'column' => ['depth' => 25],
                    'line' => [
                        'dataLabels' => [
                            'enabled' => true
                        ]
                    ],
                    'enableMouseTracking' => false,
                ],
                'xAxis' => [
                    'categories' => $report[0],
                    'labels' => [
                        'skew3d' => true,
                        'style' => ['fontSize' => '10px']
                    ]
                ],
                'yAxis' => [
                    'title' => ['text' => null]
                ],
                'series' => [
                    ['name' => 'Регистрация', 'data' => $report[1]],
                    ['name' => 'Затраты', 'data' => $report[6]],
                    ['name' => 'Затраты по ошибки', 'data' => $report[3]],
                    ['name' => 'Блокированно акк', 'data' => $report[4]],
                ],
                'credits' => ['enabled' => false],
                'legend' => ['enabled' => true],
            ]
        ]);
        ?>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-body">
                <table class="table table-bordered" style="padding:0px;">
                    <thead>
                    </thead>
                    <tbody>
                    <tr style="background: darkorange;">
                        <td><?='Итого'?></td>
                        <td><?="Регистрация: ".array_sum($report[1])?></td>
                        <td>
                            <?= "Сумма регистрации: ".array_sum($report[6])?>
                        </td>
                        <td>
                            <?= "Сумма затрат: ".array_sum($report[2])?>
                        </td>
                        <td>
                            <?= "Сумма ошибок: ".array_sum($report[3])?>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-body">
                <table class="table table-bordered" style="padding:0px;">
                    <thead>
                    </thead>
                    <tbody>
                    <tr style="background: darkorange;">
                        <td><?='Итого'?></td>
                        <td><?="Блокировок: ".array_sum($report[4])?></td>
                        <td>
                            <?= "Сумма блокировок: ".array_sum($report[5])?>
                        </td>
                        <td>
                            <?= "Блок + ошибки: ".$report[7]?>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" style="margin-top: 20px;">
                <div class="panel-heading">
                    <div class="panel-title">Статистика по финансам</div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" style="padding:0px;">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Сумма пополнений</th>
                            <th>Купили ботов</th>
                            <th>Списали за отправку</th>
                            <th>Сумма итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $counter = 1;
                        ?>
                        <tr style="background: darkorange;">
                            <td><?='Итого'?></td>
                            <td></td>
                            <td><?=array_sum($report[3])."<br/> (кол-во ".array_sum($report[2]).")"?></td>
                            <td>
                                <?php
                                $sumG1 = array_sum($report[6])*40;
                                $sumCH1 = array_sum($report[6]) *7;
                                echo array_sum($report[6])."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                                ?>
                            </td>
                            <td>
                                <?php
                                $sumG2 = array_sum($report[7]) * 1;
                                $sumCH2 = array_sum($report[7]) * 0.5;
                                echo array_sum($report[7])."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                                ?>
                            </td>
                            <td>
                                <?php
                                $finishG = $sumG1 + $sumG2;
                                $finishC = $sumCH1 + $sumCH2;
                                echo " Сумма {$finishG} / Чистыми {$finishC}";
                                ?>
                            </td>
                        </tr>
                        <?php for ($i=0; $i < count($report[0]); $i++): ?>

                            <tr>
                                <td><?=$counter?></td>
                                <td><?=$report[0][$i]?></td>
                                <td><?=$report[3][$i]."<br/> (кол-во {$report[2][$i]})"?></td>
                                <td>
                                    <?php
                                    $sumG1 = $report[6][$i]*40;
                                    $sumCH1 = $report[6][$i]*7;
                                    echo $report[6][$i]."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $sumG2 = $report[7][$i]*1;
                                    $sumCH2 = $report[7][$i]*0.5;
                                    echo $report[7][$i]."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $finishG = $sumG1 + $sumG2;
                                    $finishC = $sumCH1 + $sumCH2;
                                    echo "<br/> Сумма {$finishG} / Чистыми {$finishC}";
                                    ?>
                                </td>
                            </tr>
                            <?php $counter++; ?>
                        <?php endfor; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
