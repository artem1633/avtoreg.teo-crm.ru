<?php

use app\models\AccountingReport;
use app\models\BlockIp;
use app\models\Companies;
use app\models\DailyReport;
use app\modules\api\controllers\ClientController;
use kartik\grid\GridView;


/**
 * @var $report array
 * @var $model \app\models\RegistrationReportFilter
 */


?>


<div class="panel panel-success">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                <div class="panel-heading ui-draggable-handle">
                    <h1 class="panel-title"> <b data-introindex="5-3">Сегодня посетили систему</b>

                    </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default" style="margin-top: 20px;">
                        <div class="panel-body">
                            <table class="table table-bordered" style="padding:0px;">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Дата</th>
                                    <th>Название</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $counter = 0;
                                ?>
                                <tr style="background: darkorange;">

                                    <?php
                                    $findRep = DailyReport::find()
                                        ->andfilterWhere(['=','type', 5])
                                        ->andfilterWhere(['like','date_event', date('Y-m-d')])->all();
                                    foreach ($findRep as $item):
                                    ?>
                                    <tr>
                                        <td><?=$counter?></td>
                                        <td><?=$item->date_event?></td>
                                        <td>
                                            <?php
                                            $company = Companies::findOne($item->company_id);
                                            echo $company->company_name;
                                            ?>
                                        </td>
                                    </tr>
                                <?php $counter++; ?>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                <div class="panel-heading ui-draggable-handle">
                    <h1 class="panel-title"> <b data-introindex="5-3">Уникальные посещения</b>

                    </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'id' => 'crud-datatable-2',
                        'dataProvider' => $model,
                        //'filterModel' => $searchModel,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'columns' => [
                            'comments',
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'company_id',
                                'label' => 'Компания',
                                'content' => function($data){
                                    $listS = DailyReport::find()->where(['comments' => $data->comments])->select(['company_id'])->distinct()->all();
                                    $all = '';
                                    $i = 0;
                                    foreach ($listS as $item){
                                        $company = Companies::findOne($item->company_id);
                                        $all .= ', '.$company->company_name;
                                        $i++;
                                        if ($i > 3) {
                                            $block = new BlockIp([
                                                'key' => $data->comments,
                                                'message' => 'Вы нарушили правила пользования сервисом! Свяжитесь с администрацией в группе в вк https://vk.com/teo_target',
                                            ]);

                                            ClientController::sendTelMessage('247187885', 'Добавили новый айпи в блок лист: '. $all );
                                            $block->save(false);
                                        }
                                    }
                                    return $all;
                                }
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                        'panel' => null,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
