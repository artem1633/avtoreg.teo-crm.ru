<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\AutoRegs;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "Фото",
        'content' => function ($data) {

            return Html::img(
                $data->photo ? $data->photo :  "@web/images/user.png" ,
                ['alt' => 'message user image', 'class' => 'direct-chat-img']);


        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'content' => function ($data) {
            Yii::warning($data, __METHOD__);
            $text = $data->username ? $data->username : "{$data->link}";
            $text .= '-'.$data->getGender()[$data->gender];
            $price = '';
            if($data->price != null) $price = '<span class="pull-right"><i class="fa fa-ruble"></i> '.$data->price.'</span>';
            return Html::a($text . $price,
                    "{$data->link}", ['target' => '_blank', 'data' => ['pjax' => 0]])
                .'<br/>'.$data->date_block;

        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
        'content' => function ($data) {
            Yii::warning($data, __METHOD__);
            return $data->login.'<br/>'.$data->password;

        },
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'task_id',
        'filter' => AutoRegs::getTasksList(),
        'content' => function($data){
            if ($data->status == 0) {
                $difference = intval(abs(
                    strtotime($data->date_block) - strtotime($data->register_date)
                ));
                $difference = intval($difference / (3600 * 24));
                return $data->getTasksList()[$data->task_id] ." дней ({$difference})" ;
            }
            $difference = intval(abs(
                strtotime(date('Y-m-d')) - strtotime($data->register_date)
            ));
            $difference = intval($difference / (3600 * 24));
            return $data->getTasksList()[$data->task_id] ." дней ({$difference})" ;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => AutoRegs::getUsersList(),
        'visible' => Yii::$app->user->identity->type === 0 ? true : false,
        'content' => function($data){
            return $data->user->fio;
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {delete}',
        //'visible' => Yii::$app->user->identity->type == 0 ? true : false,
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Подтвердите действие',
            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'],
    ],

];   