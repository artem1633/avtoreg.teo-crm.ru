<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegs */
?>
<div class="auto-regs-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'password',
            'link:ntext',
            'status',
            'gender',
            'price',
            'add_information:ntext',
            'proxy_type',
            'comment:ntext',
            'register_date',
        ],
    ]) ?>

</div>
