<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegs */
?>
<div class="auto-regs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
