<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserAgent */
?>
<div class="user-agent-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
