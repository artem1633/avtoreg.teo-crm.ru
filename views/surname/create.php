<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Surname */

?>
<div class="surname-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
