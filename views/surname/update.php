<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Surname */
?>
<div class="surname-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
