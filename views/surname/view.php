<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Surname */
?>
<div class="surname-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
            'gender',
            'date',
        ],
    ]) ?>

</div>
