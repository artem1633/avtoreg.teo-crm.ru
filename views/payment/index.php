<?php
use yii\helpers\Html,
    yii\bootstrap\Modal,
    app\components\helpers\FunctionHelper,
    kartik\grid\GridView,
    app\models\AccountingReport,
    johnitvn\ajaxcrud\CrudAsset;


CrudAsset::register($this);
$this->title = 'Мои финансы';
?>

<div class="atelier-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
                    'id' => 'crud-datatable-2',
                    'dataProvider' => $dataProviderCredit,
                    //'filterModel' => $searchModel,
                    'pjax' => true,
                    'responsiveWrap' => false,
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'width' => '30px',
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'date_report',
                            'content' => function ($model) {
                                return date('H:i d.m.Y', strtotime($model->date_report) );
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'company_id',
                            'content' => function ($model) {
                                return $model->companyModel ? $model->companyModel->fio : null;
                            },
                            'visible' => Yii::$app->user->identity->type == 0,
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'amount',
                            'content' => function ($model) {
                                return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'operation_type',
                            'content' => function ($model) {
                                switch ($model->operation_type) {
                                    case AccountingReport::TYPE_INCOME_BALANCE:
                                        return 'Пополнение баланса';
                                        break;
                                    case AccountingReport::TYPE_INCOME_AFFILIATE:
                                        return 'Реферальное отчисление';
                                        break;
                                    case AccountingReport::TYPE_DISPATCH_PAYED:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_OUTPUT_AFFILIATE:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_SHOP_PAYED:
                                        return 'Оплата магазина';
                                        break;
                                    default:
                                        return null;
                                }
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'description',
                            'content' => function ($model) {
                                return $model->description;
                            }
                        ],
                    ],

                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'toolbar'=> [
                        ['content'=>  null
                        ],
                    ],
                    'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                        'before'=>'',
                        'after'=>'',
                    ]
                ]); ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>