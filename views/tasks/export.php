<?php

use app\models\Statuses;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var $model \app\models\AutoregExport */
/** @var $dispatch \app\models\Tasks */


?>


<?php $form = ActiveForm::begin(['id' => 'export-form']) ?>


<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'statuses')->widget(\kartik\select2\Select2::class, [
            'data' => ArrayHelper::map($model->getDate($dispatch->id), 'date', 'text'),
            'options' => [
                'multiple' => true,
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'count')->input('number') ?>
    </div>
</div>

<?php ActiveForm::end() ?>
