<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
?>
<div class="tasks-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'status',
            'count_autoreg',
            'time_delay',
            'important_delay',
            'time_expectations',
            'time_attempt',
            'repost_count',
            'entry_count',
            'photo_count',
            'gender',
            'youth_start',
            'youth_end',
            'fill_status',
            'fill_inform',
            'fill_job',
            'fill_residence',
            'autoreg_id',
        ],
    ]) ?>

</div>
