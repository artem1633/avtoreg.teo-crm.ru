<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
$disabled = true;
if(Yii::$app->user->identity->type == 0) $disabled = false;
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'country')->dropDownList(\app\models\Tasks::getCountries()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(),[]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'count_autoreg')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'gender')->dropDownList($model->getGender(),[]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'important_delay')->textInput(['type' => 'number', 'disabled' => $disabled]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'time_expectations')->textInput(['type' => 'number', 'disabled' => $disabled]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'time_attempt')->textInput(['type' => 'number', 'disabled' => $disabled]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'repost_count')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'entry_count')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'photo_count')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'youth_start')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'youth_end')->textInput(['type' => 'date']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'time_delay')->textInput(['type' => 'number', 'disabled' => $disabled]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'fill_status')->checkBox(['style' => 'margin-top:30px;']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fill_residence')->checkBox(['style' => 'margin-top:30px;']) ?>
        </div>
    </div>
    
    <div class="row" style="padding-top: 25px;">
        <div class="col-md-4">
            <?= $form->field($model, 'fill_inform')->checkBox() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fill_job')->checkBox() ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
