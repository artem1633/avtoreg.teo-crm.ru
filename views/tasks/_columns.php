<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Tasks;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){
            return Html::a($model->name, ['auto-regs/index', 'AutoRegsSearch[task_id]' => $model->id]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => Tasks::getStatus(),
        'content' => function($data){
            return $data->getStatus()[$data->status];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count_autoreg',
        'content' => function($data){
            $content = $data->count_autoreg . '/' . \app\models\AutoRegs::find()->where(['task_id' => $data->id])->count();

            $content .= '/'.'<span class="text-success" title="Не экспортировано">'.\app\models\AutoRegs::find()->where(['task_id' => $data->id, 'export' => false])->count().'</span>';
            $content .= ' <span class="text-danger" title="Заблокировано">('.\app\models\AutoRegs::find()->where(['task_id' => $data->id, 'export' => false, 'status' => 0])->count().')</span>';



            $content .= '/'.'<span class="text-warning" title="Экспортировано">'.\app\models\AutoRegs::find()->where(['task_id' => $data->id, 'export' => true])->count().'</span>';
            $content .= ' <span class="text-danger" title="Заблокировано">('.\app\models\AutoRegs::find()->where(['task_id' => $data->id, 'export' => true, 'status' => 0])->count().')</span>';

            return $content;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'time_delay',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'important_delay',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'country',
        'content' => function($data){
            if(isset(Tasks::getCountries()[$data->country])){
                return Tasks::getCountries()[$data->country];
            }

            return Yii::$app->formatter->asText(null);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'gender',
        'filter' => Tasks::getGender(),
        'content' => function($data){
            return $data->getGender()[$data->gender];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'photo_count',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'width' => '10px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => Tasks::getUsersList(),
        'visible' => Yii::$app->user->identity->type === 0 ? true : false,
        'content' => function($data){
            return $data->user->fio;
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'photo_count',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'gender',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'youth_start',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'youth_end',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'fill_status',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'fill_inform',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'fill_job',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'fill_residence',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'autoreg_id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'template' => '{export} {delete-blocked-auto-regs} {mark-unexport} {update} {delete}',
        //'visible' => Yii::$app->user->identity->type == 0 ? true : false,
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'buttons'=>[
            'export' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-share"></span>', ['export', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                ]);
            },
            'mark-unexport' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-copy"></span>', ['mark-unexport', 'id' => $model->id], [
                    'class' => 'btn btn-info btn-xs',
                    'role' => 'modal-remote',
                    'title'=>'Пометить все активные аккаунты как невыгруженные',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите пометить все активные аккаунты как невыгруженные?'
                ]);
            },
            'delete-blocked-auto-regs' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-user"></span>', ['delete-blocked-auto-regs', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-xs',
                    'role' => 'modal-remote',
                    'title'=>'Удалить все аккаунты данной задачи',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить все заблокированные аккаунты данной задачи?'
                ]);
            },
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   