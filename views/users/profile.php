<?php

use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Users;
use app\models\Questionary;

CrudAsset::register($this);
$model = Users::findOne(Yii::$app->user->identity->id);

    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';

?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'profile-pjax']) ?>
<div class="row">
    <div class="">
        <!-- CONTACT ITEM -->
        <div class="profile-container">
            <div class="panel panel-default">
                <div class="panel-body profile">
                    <div class="profile-image">
                        <img src="<?= $path?>" alt="Nadia Ali">
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name"><?=$model->fio?></div>
                        <div class="profile-data-title"><?=$model->getPermission()?></div>
                    </div>
                    <div class="profile-controls">
                        <?= Html::a('<span class="fa fa-pencil"></span>', ['/users/change', 'id' => $model->id], [ 'role' => 'modal-remote', 'title'=> 'Профиль','class'=>'profile-control-left']); ?>
                        <?= Html::a('<span class="fa fa-download"></span>', ['/users/avatar'], [ 'role' => 'modal-remote', 'title'=> 'Загрузить аватар','class'=>'profile-control-right']); ?>
                    </div>
                </div>                                
                <div class="panel-body">                                    
                    <div class="contact-info">
                        <p><b>ФИО</b> : <?= $model->fio ?></p> 
                        <p><b>Логин</b> : <?= $model->login ?></p>
                        <p><b>Телефон</b> : <?=$model->telephone ?></p>
                    </div>
                </div>                                
            </div>
    </div>
        <!-- END CONTACT ITEM -->
    </div>                       
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>