<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proxy */
?>
<div class="proxy-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ip',
            'type',
            'login',
            'password',
            'source',
        ],
    ]) ?>

</div>
