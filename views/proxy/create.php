<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Proxy */

?>
<div class="proxy-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
