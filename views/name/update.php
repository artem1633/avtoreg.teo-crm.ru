<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Name */
?>
<div class="name-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
