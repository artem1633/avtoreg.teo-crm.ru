<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Name */
?>
<div class="name-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'gender',
            'date',
        ],
    ]) ?>

</div>
