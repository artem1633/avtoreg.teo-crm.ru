<?php

use yii\db\Migration;

/**
 * Handles adding group to table `{{%additional_information}}`.
 */
class m190907_133348_add_group_column_to_additional_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('additional_information', 'group', $this->integer()->comment('Группа'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('additional_information', 'group');
    }
}
