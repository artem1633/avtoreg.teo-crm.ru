<?php

use yii\db\Migration;
use app\models\Tasks;

/**
 * Handles adding user_id to table `{{%tasks}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190607_173500_add_user_id_column_to_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tasks}}', 'user_id', $this->integer()->comment('Пользовател'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-tasks-user_id}}',
            '{{%tasks}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-tasks-user_id}}',
            '{{%tasks}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM tasks");
        $tasks = $command->queryAll();
        foreach ($tasks as $value) {
            Yii::$app->db->createCommand()->update('tasks', ['user_id' => 1], [ 'id' => $value['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-tasks-user_id}}',
            '{{%tasks}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-tasks-user_id}}',
            '{{%tasks}}'
        );

        $this->dropColumn('{{%tasks}}', 'user_id');
    }
}
