<?php

use yii\db\Migration;

/**
 * Class m190602_132230_change_user_type
 */
class m190602_132230_change_user_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('users', array(
            'type' => 0), 
            'id=1'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
