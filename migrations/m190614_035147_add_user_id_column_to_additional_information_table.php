<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `{{%additional_information}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190614_035147_add_user_id_column_to_additional_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%additional_information}}', 'user_id', $this->integer()->comment('Пользовател'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-additional_information-user_id}}',
            '{{%additional_information}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-additional_information-user_id}}',
            '{{%additional_information}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM additional_information");
        $additional_information = $command->queryAll();
        foreach ($additional_information as $value) {
            Yii::$app->db->createCommand()->update('additional_information', ['user_id' => 1], [ 'id' => $value['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-additional_information-user_id}}',
            '{{%additional_information}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-additional_information-user_id}}',
            '{{%additional_information}}'
        );

        $this->dropColumn('{{%additional_information}}', 'user_id');
    }
}
