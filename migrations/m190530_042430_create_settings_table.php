<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m190530_042430_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'key' => $this->string(255)->comment('Ключ'),
            'value' => $this->text()->comment('Значение'),
        ]);

        $this->insert('settings',array(
            'name' => 'Телеграмм Бот',
            'key' => 'telegram_key',
            'value' => '876813340:AAHqFY8farN-p6N22QZ4EEzxJTNc9giK6xg', 
        ));

        $this->insert('settings',array(
            'name' => 'Прокси Телеграмм',
            'key' => 'telegram_proxy',
            'value' => '45.32.155.5:35200', 
        ));

        $this->insert('settings',array(
            'name' => 'Ключ смс',
            'key' => 'key_sms',
            'value' => 'bf53dA361492fAe088320336ff1567d4', 
        ));

        $this->insert('settings',array(
            'name' => 'Кому отправлять уведомления',
            'key' => 'user_push',
            'value' => '247187885',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
