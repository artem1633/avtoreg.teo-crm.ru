<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additional_information}}`.
 */
class m190530_044130_create_additional_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%additional_information}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->comment('Тип'),
            'value' => $this->string(255)->comment('Значение'),
            'count' => $this->integer()->comment('Кол-во раз использовалась'),
            'last_date' => $this->datetime()->comment('Дата и время последнего использования'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%additional_information}}');
    }
}
