<?php

use yii\db\Migration;

/**
 * Handles adding country to table `{{%tasks}}`.
 */
class m190916_172518_add_country_column_to_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'country', $this->integer()->comment('Страна'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks', 'country');
    }
}
