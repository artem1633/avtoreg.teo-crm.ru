<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `{{%logs}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190613_165056_add_user_id_column_to_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%logs}}', 'user_id', $this->integer()->comment('Пользовател'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-logs-user_id}}',
            '{{%logs}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-logs-user_id}}',
            '{{%logs}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM logs");
        $logs = $command->queryAll();
        foreach ($logs as $value) {
            Yii::$app->db->createCommand()->update('logs', ['user_id' => 1], [ 'id' => $value['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-logs-user_id}}',
            '{{%logs}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-logs-user_id}}',
            '{{%logs}}'
        );

        $this->dropColumn('{{%logs}}', 'user_id');
    }
}
