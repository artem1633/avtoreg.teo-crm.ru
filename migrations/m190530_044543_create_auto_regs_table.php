<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auto_regs}}`.
 */
class m190530_044543_create_auto_regs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auto_regs}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(255)->comment('Логин'),
            'password' => $this->string(255)->comment('Пароль'),
            'link' => $this->text()->comment('Ссылка на страница'),
            'status' => $this->integer()->comment('Статус доступности'),
            'gender' => $this->integer()->comment('Пол'),
            'add_information' => $this->text()->comment('Доп. информация'),
            'proxy_type' => $this->integer()->comment('Тип прокси'),
            'comment' => $this->text()->comment('Комментарий'),
            'register_date' => $this->datetime()->comment('Дата и ввремя регистрации'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%auto_regs}}');
    }
}
