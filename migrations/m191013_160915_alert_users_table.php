<?php

use yii\db\Migration;

/**
 * Class m191013_160915_alert_users_table
 */
class m191013_160915_alert_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'sms_key', $this->string()->comment('СМС-ключ'));
        $this->addColumn('users', 'one_price', $this->float()->comment('Стоимость одного'));
        $this->addColumn('users', 'mobile_proxy', $this->boolean()->comment('Мобильный прокси'));
        $this->addColumn('users', 'mp_login', $this->string()->comment('Мобильный прокси логин'));
        $this->addColumn('users', 'mp_password', $this->string()->comment('Мобильный прокси пароль'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'sms_key');
        $this->dropColumn('users', 'one_price');
        $this->dropColumn('users', 'mobile_proxy');
        $this->dropColumn('users', 'mp_login');
        $this->dropColumn('users', 'mp_password');
    }
}
