<?php

use yii\db\Migration;

/**
 * Class m190916_175548_add_new_settings
 */
class m190916_175548_add_new_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'mobile_proxies',
            'name' => 'Прокси мобильные'
        ]);

        $this->insert('settings', [
            'key' => 'mobile_proxies_login',
            'name' => 'Мобильный проксили логин'
        ]);

        $this->insert('settings', [
            'key' => 'mobile_proxies_password',
            'name' => 'МП пароль'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => ['mobile_proxies_password', 'mobile_proxies_login', 'mobile_proxies']]);
    }
}
