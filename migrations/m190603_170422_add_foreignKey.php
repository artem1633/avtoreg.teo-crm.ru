<?php

use yii\db\Migration;

/**
 * Class m190603_170422_add_foreignKey
 */
class m190603_170422_add_foreignKey extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-auto_regs-task_id', 'auto_regs', 'task_id', false);
        $this->addForeignKey("fk-auto_regs-task_id", "auto_regs", "task_id", "tasks", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-auto_regs-task_id','auto_regs');
        $this->dropIndex('idx-auto_regs-task_id','auto_regs');
    }
}
