<?php

use yii\db\Migration;

/**
 * Class m190615_164702_create_table_daily_report
 */
class m190615_164702_create_table_daily_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('daily_report', [
            'id' => $this->primaryKey(),
            'date_event' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время события"',
            'user_id' => $this->integer()->notNull()->comment('ID компании'),
            'task_id' => $this->integer()->notNull()->defaultValue(0)->comment('Тип'),
            'type' => $this->string()->notNull()->defaultValue(0)->comment('Тип'),
            'comment' => $this->string()->notNull()->defaultValue(0)->comment('Тип'),
            'comment2' => $this->string()->notNull()->defaultValue(0)->comment('Тип'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('daily_report');
    }
}
