<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%surname}}`.
 */
class m190530_044820_create_surname_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%surname}}', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(255)->comment('Фамилия'),
            'gender' => $this->integer()->comment('Пол'),
            'date' => $this->date()->comment('Дата использования'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%surname}}');
    }
}
