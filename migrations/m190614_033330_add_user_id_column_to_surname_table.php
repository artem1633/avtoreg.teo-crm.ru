<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `{{%surname}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190614_033330_add_user_id_column_to_surname_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%surname}}', 'user_id', $this->integer()->comment('Пользовател'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-surname-user_id}}',
            '{{%surname}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-surname-user_id}}',
            '{{%surname}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM surname");
        $surname = $command->queryAll();
        foreach ($surname as $value) {
            Yii::$app->db->createCommand()->update('surname', ['user_id' => 1], [ 'id' => $value['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-surname-user_id}}',
            '{{%surname}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-surname-user_id}}',
            '{{%surname}}'
        );

        $this->dropColumn('{{%surname}}', 'user_id');
    }
}
