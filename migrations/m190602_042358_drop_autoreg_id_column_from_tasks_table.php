<?php

use yii\db\Migration;

/**
 * Handles dropping autoreg_id from table `{{%tasks}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%auto_reg}}`
 */
class m190602_042358_drop_autoreg_id_column_from_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%auto_reg}}`
        $this->dropForeignKey(
            '{{%fk-tasks-autoreg_id}}',
            '{{%tasks}}'
        );

        // drops index for column `autoreg_id`
        $this->dropIndex(
            '{{%idx-tasks-autoreg_id}}',
            '{{%tasks}}'
        );

        $this->dropColumn('{{%tasks}}', 'autoreg_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%tasks}}', 'autoreg_id', $this->integer());

        // creates index for column `autoreg_id`
        $this->createIndex(
            '{{%idx-tasks-autoreg_id}}',
            '{{%tasks}}',
            'autoreg_id'
        );

        // add foreign key for table `{{%auto_reg}}`
        $this->addForeignKey(
            '{{%fk-tasks-autoreg_id}}',
            '{{%tasks}}',
            'autoreg_id',
            '{{%auto_reg}}',
            'id',
            'CASCADE'
        );
    }
}
