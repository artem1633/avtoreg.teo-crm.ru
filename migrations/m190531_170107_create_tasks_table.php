<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tasks}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%auto_regs}}`
 */
class m190531_170107_create_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tasks}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
            'status' => $this->integer()->comment('Статус'),
            'count_autoreg' => $this->float()->comment('Кол-во авторегов'),
            'time_delay' => $this->float()->comment('Время задержки между каждым действием бота'),
            'important_delay' => $this->float()->comment('Важные задержки'),
            'time_expectations' => $this->float()->comment('Время ожидания смс'),
            'time_attempt' => $this->float()->comment('Время каждой попытки'),
            'repost_count' => $this->float()->comment('Кол-во репостов'),
            'entry_count' => $this->float()->comment('Кол-во вступлений'),
            'photo_count' => $this->float()->comment('Кол-во фото'),
            'gender' => $this->integer()->comment('Пол'),
            'youth_start' => $this->date()->comment('Возраст старт'),
            'youth_end' => $this->date()->comment('Возраст конец'),
            'fill_status' => $this->boolean()->comment('Заполнять статус'),
            'fill_inform' => $this->boolean()->comment('Заполнять информация о себе'),
            'fill_job' => $this->boolean()->comment('Заполнять место работы'),
            'fill_residence' => $this->boolean()->comment('Заполнять место жительства'),
            'autoreg_id' => $this->integer()->comment('Авторег'),
        ]);

        // creates index for column `autoreg_id`
        $this->createIndex(
            '{{%idx-tasks-autoreg_id}}',
            '{{%tasks}}',
            'autoreg_id'
        );

        // add foreign key for table `{{%auto_regs}}`
        $this->addForeignKey(
            '{{%fk-tasks-autoreg_id}}',
            '{{%tasks}}',
            'autoreg_id',
            '{{%auto_regs}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%auto_regs}}`
        $this->dropForeignKey(
            '{{%fk-tasks-autoreg_id}}',
            '{{%tasks}}'
        );

        // drops index for column `autoreg_id`
        $this->dropIndex(
            '{{%idx-tasks-autoreg_id}}',
            '{{%tasks}}'
        );

        $this->dropTable('{{%tasks}}');
    }
}
