<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_agent}}`.
 */
class m190531_052835_create_user_agent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_agent}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_agent}}');
    }
}
