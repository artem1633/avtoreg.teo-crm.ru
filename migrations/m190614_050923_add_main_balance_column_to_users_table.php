<?php

use yii\db\Migration;

/**
 * Handles adding main_balance to table `{{%users}}`.
 */
class m190614_050923_add_main_balance_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users}}', 'main_balance', $this->float()->comment('Баланс основной'));
        $this->addColumn('{{%users}}', 'partner_balance', $this->float()->comment('Баланс партнерский'));
        $this->addColumn('{{%users}}', 'referal_id', $this->integer()->comment('Реферал'));
        $this->addColumn('{{%users}}', 'prosent_referal', $this->float()->comment('Процент с оплат рефералов'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users}}', 'main_balance');
        $this->dropColumn('{{%users}}', 'partner_balance');
        $this->dropColumn('{{%users}}', 'referal_id');
        $this->dropColumn('{{%users}}', 'prosent_referal');
    }
}
