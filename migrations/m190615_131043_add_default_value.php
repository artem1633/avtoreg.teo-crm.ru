<?php

use yii\db\Migration;

/**
 * Class m190615_131043_add_default_value
 */
class m190615_131043_add_default_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'name' => 'Минимальная цена смса',
            'key' => 'minimal_message_price',
            'value' => '0', 
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
