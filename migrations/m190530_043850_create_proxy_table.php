<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%proxy}}`.
 */
class m190530_043850_create_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%proxy}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(255)->comment('Ip'),
            'type' => $this->integer()->comment('Тип'),
            'login' => $this->string(255)->comment('Логин'),
            'password' => $this->string(255)->comment('Пароль'),
            'source' => $this->string(255)->comment('Источник'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%proxy}}');
    }
}
