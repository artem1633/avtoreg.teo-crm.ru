<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `{{%auto_regs}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190613_162940_add_user_id_column_to_auto_regs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%auto_regs}}', 'user_id', $this->integer()->comment('Пользовател'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-auto_regs-user_id}}',
            '{{%auto_regs}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-auto_regs-user_id}}',
            '{{%auto_regs}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM auto_regs");
        $auto_regs = $command->queryAll();
        foreach ($auto_regs as $value) {
            Yii::$app->db->createCommand()->update('auto_regs', ['user_id' => 1], [ 'id' => $value['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-auto_regs-user_id}}',
            '{{%auto_regs}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-auto_regs-user_id}}',
            '{{%auto_regs}}'
        );

        $this->dropColumn('{{%auto_regs}}', 'user_id');
    }
}
