<?php

use yii\db\Migration;

/**
 * Class m190603_170335_drop_foreignKey
 */
class m190603_170335_drop_foreignKey extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%auto_regs}}`
        $this->dropForeignKey(
            '{{%fk-auto_regs-task_id}}',
            '{{%auto_regs}}'
        );

        // drops index for column `task_id`
        $this->dropIndex(
            '{{%idx-auto_regs-task_id}}',
            '{{%auto_regs}}'
        );
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // creates index for column `task_id`
        $this->createIndex(
            '{{%idx-auto_regs-task_id}}',
            '{{%auto_regs}}',
            'task_id'
        );

        // add foreign key for table `{{%auto_regs}}`
        $this->addForeignKey(
            '{{%fk-auto_regs-task_id}}',
            '{{%auto_regs}}',
            'task_id',
            '{{%auto_regs}}',
            'id',
            'CASCADE'
        );
    }

}
