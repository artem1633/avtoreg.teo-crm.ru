<?php

use yii\db\Migration;

/**
 * Class m190605_204501_add_colums_auto_regs
 */
class m190605_204501_add_colums_auto_regs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('auto_regs', 'photo', $this->text());
        $this->addColumn('auto_regs', 'username', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('auto_regs', 'photo');
        $this->dropColumn('auto_regs', 'username');
    }
}
