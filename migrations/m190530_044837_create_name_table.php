<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%name}}`.
 */
class m190530_044837_create_name_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%name}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Имя'),
            'gender' => $this->integer()->comment('Пол'),
            'date' => $this->date()->comment('Дата использования'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%name}}');
    }
}
