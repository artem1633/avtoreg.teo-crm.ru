<?php

use yii\db\Migration;

/**
 * Handles adding price to table `{{%auto_regs}}`.
 */
class m190615_123234_add_price_column_to_auto_regs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%auto_regs}}', 'price', $this->float()->comment('Цена'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%auto_regs}}', 'price');
    }
}
