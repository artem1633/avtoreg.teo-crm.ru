<?php

use yii\db\Migration;

/**
 * Class m190602_173057_add_colums_autoreg
 */
class m190602_173057_add_colums_autoreg extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('auto_regs', 'export', $this->boolean()->defaultValue(false));
        $this->addColumn('auto_regs', 'date_block', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('auto_regs', 'export');
        $this->dropColumn('auto_regs', 'date_block');
    }
}
