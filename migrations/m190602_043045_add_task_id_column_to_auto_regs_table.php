<?php

use yii\db\Migration;

/**
 * Handles adding task_id to table `{{%auto_regs}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%auto_regs}}`
 */
class m190602_043045_add_task_id_column_to_auto_regs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%auto_regs}}', 'task_id', $this->integer()->comment('Задача'));

        // creates index for column `task_id`
        $this->createIndex(
            '{{%idx-auto_regs-task_id}}',
            '{{%auto_regs}}',
            'task_id'
        );

        // add foreign key for table `{{%auto_regs}}`
        $this->addForeignKey(
            '{{%fk-auto_regs-task_id}}',
            '{{%auto_regs}}',
            'task_id',
            '{{%auto_regs}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%auto_regs}}`
        $this->dropForeignKey(
            '{{%fk-auto_regs-task_id}}',
            '{{%auto_regs}}'
        );

        // drops index for column `task_id`
        $this->dropIndex(
            '{{%idx-auto_regs-task_id}}',
            '{{%auto_regs}}'
        );

        $this->dropColumn('{{%auto_regs}}', 'task_id');
    }
}
