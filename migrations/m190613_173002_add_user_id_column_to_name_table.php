<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `{{%name}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m190613_173002_add_user_id_column_to_name_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%name}}', 'user_id', $this->integer()->comment('Пользовател'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-name-user_id}}',
            '{{%name}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-name-user_id}}',
            '{{%name}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM name");
        $name = $command->queryAll();
        foreach ($name as $value) {
            Yii::$app->db->createCommand()->update('name', ['user_id' => 1], [ 'id' => $value['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-name-user_id}}',
            '{{%name}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-name-user_id}}',
            '{{%name}}'
        );

        $this->dropColumn('{{%name}}', 'user_id');
    }
}
