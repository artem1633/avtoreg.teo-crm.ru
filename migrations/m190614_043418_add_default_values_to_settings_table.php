<?php

use yii\db\Migration;

/**
 * Class m190614_043418_add_default_values_to_settings_table
 */
class m190614_043418_add_default_values_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'name' => 'Стоимость одного авторега',
            'key' => 'cost_autoreg',
            'value' => '0', 
        ));

        $this->insert('settings',array(
            'name' => 'Бонус за регистрацию',
            'key' => 'registr_bonus',
            'value' => '0', 
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
